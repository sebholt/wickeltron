/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <svs/sequence/clerk.hpp>

namespace svs::sequence
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Machine-Sequence" )
, _event_pool_program_sequence ( office_io ().epool_tracker () )
, _event_pool_program_run ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< svs::sequence::Clerk > ();
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _prop.startable, state ().startable () ) ) {
    emit startableChanged ();
  }
  if ( sev::change ( _prop.running, state ().is_running () ) ) {
    emit runningChanged ();
  }

  updateBlockReasons ();
}

void
Panel::request_sequence ( const Sequence & sequence_n )
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_program_sequence );
    event->sequence () = sequence_n;
    office_io ().submit ( event );
  }
}

void
Panel::setupSequence ( double speed_rpm_n, double lead_n, int rounds_n )
{
  Sequence sequence;
  sequence.section ( 1 ).set_lead ( lead_n );
  sequence.section ( 1 ).set_rounds ( rounds_n );
  sequence.set_speed_rpm ( speed_rpm_n );
  request_sequence ( sequence );
}

void
Panel::routeBegin ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_program_run );
    event->set_request ( dash::event::out::Request_Run::Request::START );
    office_io ().submit ( event );
  }
}

void
Panel::routeAbort ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_program_run );
    event->set_request ( dash::event::out::Request_Run::Request::ABORT );
    office_io ().submit ( event );
  }
}

void
Panel::updateBlockReasons ()
{
  if ( _prop.blocks == state ().blocks () ) {
    return;
  }

  _prop.blocks = state ().blocks ();
  _prop.blockReasons.clear ();

  using Block = State::Block;
  if ( _prop.blocks.test_any ( Block::INVALID ) ) {
    _prop.blockReasons.append ( "State invalid" );
  }
  if ( _prop.blocks.test_any ( Block::ROUTING_IN_PROGRESS ) ) {
    _prop.blockReasons.append ( "Routing in progress" );
  }
  if ( _prop.blocks.test_any ( Block::CONTROLLERS_UNAVAILABLE ) ) {
    _prop.blockReasons.append ( "Controllers unavailable" );
  }
  if ( _prop.blocks.test_any ( Block::WAGON_AXIS_UNALIGNED ) ) {
    _prop.blockReasons.append ( "Wagon axis unaligned" );
  }
  if ( _prop.blocks.test_any ( Block::SEQUENCE_INVALID_ROUNDS ) ) {
    _prop.blockReasons.append ( "Sequence rounds invalid" );
  }
  if ( _prop.blocks.test_any ( Block::SEQUENCE_INVALID_LENGTH ) ) {
    _prop.blockReasons.append ( "Sequence length invalid" );
  }
  if ( _prop.blocks.test_any ( Block::SEQUENCE_WAGON_UNDER_MIN ) ) {
    _prop.blockReasons.append ( "Sequence ends below axis minimum" );
  }
  if ( _prop.blocks.test_any ( Block::SEQUENCE_WAGON_OVER_MAX ) ) {
    _prop.blockReasons.append ( "Sequence ends above axis maximum" );
  }

  emit blockReasonsChanged ();
}

} // namespace svs::sequence
