/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/ctl/axis_feed/office/client.hpp>
#include <snc/svs/fac/axis_feed/office/client.hpp>
#include <snc/svs/fac/mpath/office/client_n.hpp>
#include <svs/sequence/dash_events.hpp>
#include <svs/sequence/mpath_stream.hpp>
#include <svs/sequence/state.hpp>
#include <memory>

namespace svs::sequence
{

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  struct Dash_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  struct Request
  {
    static constexpr std::uint8_t BEGIN = ( 1 << 0 );
    static constexpr std::uint8_t BREAK = ( 1 << 1 );
  };

  struct Sequence
  {
    static constexpr std::uint8_t NONE = 0;

    static constexpr std::uint8_t BEGIN = 1;

    static constexpr std::uint8_t FEED_LOCK = 2;
    static constexpr std::uint8_t MPATH_LOCK = 3;

    static constexpr std::uint8_t FEED_PULL = 4;
    static constexpr std::uint8_t FEED_PULL_WAIT = 5;

    static constexpr std::uint8_t FEED_FORWARD = 6;
    static constexpr std::uint8_t FEED_FORWARD_WAIT = 7;

    static constexpr std::uint8_t ROUTE_BEGIN = 8;
    static constexpr std::uint8_t ROUTE_WAIT = 9;

    static constexpr std::uint8_t MPATH_UNLOCK = 10;
    static constexpr std::uint8_t FEED_UNLOCK = 11;

    static constexpr std::uint8_t DONE = 12;
  };

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_request_sequence ( Dash_Event & event_n );

  void
  dash_event_request_run ( Dash_Event & event_n );

  void
  dash_event_request_run_begin ();

  void
  dash_event_request_run_abort ();

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  bool
  process_sequence ();

  void
  process_sequence_abort ();

  protected:
  // -- Utility

  void
  route_begin ();

  void
  route_break ();

  void
  route_end ();

  void
  state_update_blocks ();

  void
  state_changed ();

  private:
  // -- State
  snc::svs::ctl::actor::office::Client_Actor _actor;
  State _state;
  std::uint_fast32_t _sequence = Sequence::NONE;

  // -- Requests
  sev::mem::Flags_Fast8 _route_requests;

  // -- Resources
  snc::svs::fac::axis_feed::Mode _feed_mode_forward;
  snc::svs::fac::axis_feed::Mode _feed_mode_backward;
  snc::svs::fac::axis_feed::office::Client _feed_pilot;
  snc::svs::ctl::axis_feed::office::Client _feed_maker;
  snc::svs::fac::axis_feed::office::Client::Control _feed_pilot_control;
  snc::svs::fac::mpath::office::Client_N< 2 > _mpath_pilot;
  snc::svs::fac::mpath::office::Client_N< 2 >::Control _mpath_pilot_control;
  std::shared_ptr< MPath_Stream > _mpath_stream_sequence;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace svs::sequence
