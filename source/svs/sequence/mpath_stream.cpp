/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "mpath_stream.hpp"
#include <sev/assert.hpp>

namespace svs::sequence
{

MPath_Stream::MPath_Stream () = default;

void
MPath_Stream::setup_sequence ( const Sequence & sequence_n )
{
  _sequence = sequence_n;
}

void
MPath_Stream::setup_translation ( const sev::lag::Vector2d & translation_n )
{
  _translation = translation_n;
}

bool
MPath_Stream::open ()
{
  if ( is_open () ) {
    return true;
  }

  if ( _sequence.is_valid () ) {
    _next_section_index = 0;
  } else {
    _next_section_index = _sequence.num_sections ();
  }
  _speed_sent = false;

  _pos_cur.fill ( 0.0 );
  set_state_good ();

  return is_open ();
}

void
MPath_Stream::close ()
{
  if ( !is_open () ) {
    return;
  }

  set_state_closed ();
}

snc::mpath::Element *
MPath_Stream::read ()
{
  DEBUG_ASSERT ( is_good () );

  while ( _next_section_index < _sequence.num_sections () ) {
    Sequence_Section & section = _sequence.section ( _next_section_index );
    if ( ( section.rounds () == 0 ) || ( section.lead () < 1.0e-6 ) ) {
      ++_next_section_index;
      continue;
    }

    const double drounds = section.rounds ();
    sev::lag::Vector2d vdelta;
    vdelta[ 0 ] = -drounds * section.lead ();
    vdelta[ 1 ] = drounds * sev::lag::two_pi_d;

    // Send speed
    if ( !_speed_sent ) {
      _speed_sent = true;
      const double speed_radians =
          ( _sequence.speed_rpm () * sev::lag::two_pi_d / 60.0 );
      const double scale = ( speed_radians / vdelta[ 1 ] );
      vdelta *= scale;
      const double speed = sev::lag::magnitude ( vdelta );
      auto & cspeed = _element_variant.emplace< snc::mpath::elem::Speed > ();
      cspeed.speed ().set_value ( speed );
      return &cspeed;
    }

    // Send target after updating the state
    _speed_sent = false;
    ++_next_section_index;
    _pos_cur += vdelta;

    auto & curve =
        _element_variant.emplace< snc::mpath::d2::elem::Curve_Linear > ();
    curve.set_pos_end ( _pos_cur );
    curve.translate ( _translation );
    return &curve;
  }

  set_state_finished ();
  return &_element_variant.emplace< snc::mpath::elem::Stream_End > ();
}

} // namespace svs::sequence
