/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/lag/vector2.hpp>
#include <snc/mpath/d2/types.hpp>
#include <snc/mpath_stream/read.hpp>
#include <svs/sequence/sequence.hpp>
#include <cstdint>
#include <variant>

namespace svs::sequence
{

class MPath_Stream : public snc::mpath_stream::Read
{
  public:
  // -- Construction

  MPath_Stream ();

  // -- Setup

  void
  setup_sequence ( const Sequence & sequence_n );

  void
  setup_translation ( const sev::lag::Vector2d & translation_n );

  // --- Abstract interface

  bool
  open () override;

  void
  close () override;

  snc::mpath::Element *
  read () override;

  private:
  // -- Attributes
  bool _speed_sent = false;
  std::size_t _next_section_index = 0;
  sev::lag::Vector2d _pos_cur;
  sev::lag::Vector2d _translation;
  Sequence _sequence;

  std::variant< snc::mpath::elem::Speed,
                snc::mpath::d2::elem::Curve_Linear,
                snc::mpath::elem::Stream_End >
      _element_variant;
};

} // namespace svs::sequence
