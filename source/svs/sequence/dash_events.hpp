/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <svs/sequence/sequence.hpp>
#include <svs/sequence/state.hpp>

namespace svs::sequence::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, svs::sequence::State >;

} // namespace svs::sequence::dash::event::in

namespace svs::sequence::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t REQUEST_SEQUENCE = 0;
  static constexpr std::uint_fast32_t REQUEST_RUN = 1;
};

using Out = snc::svp::dash::event::Out;

class Request_Sequence : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST_SEQUENCE;

  Request_Sequence ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _sequence.reset ();
  }

  Sequence &
  sequence ()
  {
    return _sequence;
  }

  const Sequence &
  sequence () const
  {
    return _sequence;
  }

  private:
  Sequence _sequence;
};

/// @brief Routing begin / abort request
///
class Request_Run : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST_RUN;

  enum class Request
  {
    NONE,
    START,
    ABORT
  };

  Request_Run ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _request = Request::NONE;
  }

  Request
  request () const
  {
    return _request;
  }

  void
  set_request ( Request request_n )
  {
    _request = request_n;
  }

  private:
  Request _request;
};

} // namespace svs::sequence::dash::event::out
