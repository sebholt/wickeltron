/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/assert.hpp>
#include <cstdint>

namespace svs::sequence
{

class Sequence_Section
{
  public:
  // -- Construction

  void
  reset ()
  {
    _lead = 1.0;
    _rounds = 0;
  }

  // -- Lead

  double
  lead () const
  {
    return _lead;
  }

  void
  set_lead ( double lead_n )
  {
    _lead = lead_n;
  }

  // -- Rounds

  std::uint_fast32_t
  rounds () const
  {
    return _rounds;
  }

  void
  set_rounds ( std::uint_fast32_t rounds_n )
  {
    _rounds = rounds_n;
  }

  public:
  // -- Attributes
  double _lead = 1.0;
  std::uint_fast32_t _rounds = 0;
};

} // namespace svs::sequence
