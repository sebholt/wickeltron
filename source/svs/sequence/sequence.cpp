/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "sequence.hpp"

namespace svs::sequence
{

Sequence::Sequence () = default;

void
Sequence::reset ()
{
  for ( auto & section : _sections ) {
    section.reset ();
  }
  _speed_rpm = 0.0;
}

std::uint_fast32_t
Sequence::rounds_total () const
{
  std::uint_fast32_t rounds = 0;
  for ( auto & section : _sections ) {
    rounds += section.rounds ();
  }
  return rounds;
}

double
Sequence::length_total () const
{
  double length = 0.0;
  for ( auto & section : _sections ) {
    length += double ( section.rounds () ) * section.lead ();
  }
  return length;
}

bool
Sequence::is_valid () const
{
  if ( rounds_total () == 0 ) {
    return false;
  }
  if ( length_total () < 1.0e-6 ) {
    return false;
  }
  if ( speed_rpm () < 1.0e-6 ) {
    return false;
  }
  return true;
}

} // namespace svs::sequence
