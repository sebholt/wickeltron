/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <svs/sequence/sequence_section.hpp>
#include <array>

namespace svs::sequence
{

class Sequence
{
  public:
  // -- Construction

  Sequence ();

  void
  reset ();

  // -- Sections

  std::size_t
  num_sections () const
  {
    return _sections.size ();
  }

  const Sequence_Section &
  section ( std::size_t index_n ) const
  {
    return _sections[ index_n ];
  }

  Sequence_Section &
  section ( std::size_t index_n )
  {
    return _sections[ index_n ];
  }

  std::array< Sequence_Section, 3 > &
  sections ()
  {
    return _sections;
  }

  const std::array< Sequence_Section, 3 > &
  sections () const
  {
    return _sections;
  }

  // -- Speed

  double
  speed_rpm () const
  {
    return _speed_rpm;
  }

  void
  set_speed_rpm ( double speed_n )
  {
    _speed_rpm = speed_n;
  }

  // -- Meta

  std::uint_fast32_t
  rounds_total () const;

  double
  length_total () const;

  bool
  is_valid () const;

  public:
  // -- Attributes
  std::array< Sequence_Section, 3 > _sections;
  double _speed_rpm = 0.0;
};

} // namespace svs::sequence
