/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <svs/sequence/dash_events.hpp>
#include <svs/sequence/state.hpp>
#include <array>
#include <deque>

namespace svs::sequence
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  // -- Properties
  Q_PROPERTY ( bool startable READ startable NOTIFY startableChanged )
  Q_PROPERTY ( bool running READ running NOTIFY runningChanged )
  Q_PROPERTY (
      QStringList blockReasons READ blockReasons NOTIFY blockReasonsChanged )

  public:
  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- State interface

  const State
  state () const
  {
    return _state;
  }

  // -- State change

  void
  state_changed () override;

  // -- Prop: Startable

  bool
  startable () const
  {
    return _prop.startable;
  }

  Q_SIGNAL
  void
  startableChanged ();

  // -- Prop: running

  bool
  running () const
  {
    return _prop.running;
  }

  Q_SIGNAL
  void
  runningChanged ();

  // -- Prop: blockReasons

  const QStringList &
  blockReasons () const
  {
    return _prop.blockReasons;
  }

  Q_SIGNAL
  void
  blockReasonsChanged ();

  // -- Request interface

  void
  request_sequence ( const Sequence & sequence_n );

  Q_INVOKABLE
  void
  setupSequence ( double speed_rpm_n, double lead_n, int rounds_n );

  Q_INVOKABLE
  void
  routeBegin ();

  Q_INVOKABLE
  void
  routeAbort ();

  private:
  // -- Utility

  void
  updateBlockReasons ();

  private:
  // -- State
  State _state;

  // -- Attributes
  struct
  {
    bool startable = false;
    bool running = false;
    sev::mem::Flags_Fast8 blocks;
    QStringList blockReasons;
  } _prop;

  // -- Office event io
  sev::event::Pool< dash::event::out::Request_Sequence >
      _event_pool_program_sequence;
  sev::event::Pool< dash::event::out::Request_Run > _event_pool_program_run;
};

} // namespace svs::sequence
