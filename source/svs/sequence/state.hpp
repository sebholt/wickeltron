/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/mem/flags.hpp>
#include <svs/sequence/sequence.hpp>
#include <array>
#include <cstdint>

namespace svs::sequence
{

class State
{
  public:
  // -- Types

  enum class Stream_State : std::uint8_t
  {
    NONE,
    PREPARE,
    RUNNING,
    FINISHED,
  };

  struct Block
  {
    /// @brief Invalid state (needs update)
    static constexpr std::uint8_t INVALID = ( 1 << 0 );
    static constexpr std::uint8_t ROUTING_IN_PROGRESS = ( 1 << 1 );
    static constexpr std::uint8_t CONTROLLERS_UNAVAILABLE = ( 1 << 2 );
    static constexpr std::uint8_t WAGON_AXIS_UNALIGNED = ( 1 << 3 );
    static constexpr std::uint8_t SEQUENCE_INVALID_ROUNDS = ( 1 << 4 );
    static constexpr std::uint8_t SEQUENCE_INVALID_LENGTH = ( 1 << 5 );
    static constexpr std::uint8_t SEQUENCE_WAGON_UNDER_MIN = ( 1 << 6 );
    static constexpr std::uint8_t SEQUENCE_WAGON_OVER_MAX = ( 1 << 7 );
  };

  // -- Setup

  void
  reset ()
  {
    _blocks = Block::INVALID;
    _is_running = false;
    _is_aborting = false;
    _sequence.reset ();
  }

  // -- Blocks

  bool
  startable () const
  {
    return _blocks.is_empty ();
  }

  sev::mem::Flags_Fast8 &
  blocks ()
  {
    return _blocks;
  }

  const sev::mem::Flags_Fast8 &
  blocks () const
  {
    return _blocks;
  }

  // -- Run state

  bool
  is_running () const
  {
    return _is_running;
  }

  void
  set_is_running ( bool flag_n )
  {
    _is_running = flag_n;
  }

  // -- Abort state

  bool
  is_aborting () const
  {
    return _is_aborting;
  }

  void
  set_is_aborting ( bool flag_n )
  {
    _is_aborting = flag_n;
  }

  // -- Sequence

  Sequence &
  sequence ()
  {
    return _sequence;
  }

  const Sequence &
  sequence () const
  {
    return _sequence;
  }

  public:
  // -- Attributes
  sev::mem::Flags_Fast8 _blocks = Block::INVALID;
  bool _is_running = false;
  bool _is_aborting = false;
  Sequence _sequence;
};

} // namespace svs::sequence
