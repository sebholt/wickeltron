/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <snc/device/state/state.hpp>
#include <cstdint>

namespace svs::sequence
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Machine-Sequence" )
, _actor ( this )
, _feed_pilot ( this, 2 )
, _feed_maker ( this, _feed_pilot.axis_index () )
, _mpath_pilot ( this )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 4 );

  // Setup feed modes
  {
    using Mode = snc::svs::fac::axis_feed::Mode;
    _feed_mode_forward.set_dir ( Mode::Dir::FORWARD );
    _feed_mode_forward.speed_ref ().set_speed ( 10.0 );
    _feed_mode_forward.speed_ref ().set_accel ( 30.0 );
    _feed_mode_forward.speed_ref ().set_forward ( true );

    _feed_mode_backward = _feed_mode_forward;
    _feed_mode_backward.set_dir ( Mode::Dir::BACKWARD );
    _feed_mode_backward.speed_ref ().set_forward (
        !_feed_mode_forward.speed ().forward () );
  }

  // Setup actor interface
  _actor.set_break_hard_func ( [ this ] () { route_break (); } );

  _feed_maker.session ().set_begin_callback ( [ this ] () {
    auto ctl = _feed_maker.control ();
    if ( !ctl.set_default_mode ( _feed_mode_forward ) ) {
      throw std::runtime_error ( "Forward feed mode setting failed" );
    }
    if ( !ctl.set_default_mode ( _feed_mode_backward ) ) {
      throw std::runtime_error ( "Backward feed mode setting failed" );
    }
  } );

  _feed_maker.session ().set_end_callback (
      [ this ] () { _feed_pilot_control.release (); } );

  _mpath_pilot.session ().set_end_callback (
      [ this ] () { _mpath_pilot_control.release (); } );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _actor.connect_required ();
  _feed_pilot.connect_required ();
  _feed_maker.connect_required ();
  _mpath_pilot.connect_required ();

  _mpath_stream_sequence = std::make_shared< MPath_Stream > ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Type::REQUEST_SEQUENCE:
    dash_event_request_sequence ( event_n );
    break;
  case dash::event::out::Type::REQUEST_RUN:
    dash_event_request_run ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::dash_event_request_sequence ( Dash_Event & event_n )
{
  auto & cevent =
      static_cast< const dash::event::out::Request_Sequence & > ( event_n );

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Incoming sequence request:\n";
    logo << "  Speed: " << cevent.sequence ().speed_rpm () << " rpm\n";
    logo << "  Rounds: " << cevent.sequence ().section ( 1 ).rounds () << "\n";
    logo << "  Lead: " << cevent.sequence ().section ( 1 ).lead () << " μm\n";
  }

  _state.sequence () = cevent.sequence ();
  state_changed ();
}

void
Clerk::dash_event_request_run ( Dash_Event & event_n )
{
  using Request = dash::event::out::Request_Run::Request;

  auto & cevent =
      static_cast< const dash::event::out::Request_Run & > ( event_n );
  switch ( cevent.request () ) {
  case Request::START:
    dash_event_request_run_begin ();
    break;
  case Request::ABORT:
    dash_event_request_run_abort ();
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::dash_event_request_run_begin ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Incoming route begin request." );
  if ( _state.is_running () ) {
    return;
  }

  if ( !_route_requests.test_any_set ( Request::BEGIN ) ) {
    request_processing ();
  }
}

void
Clerk::dash_event_request_run_abort ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Incoming route abort request." );
  // Clear route begin requests
  _route_requests.unset ( Request::BEGIN );
  // Abort rounning routing
  if ( _state.is_running () ) {
    if ( !_route_requests.test_any_set ( Request::BREAK ) ) {
      request_processing ();
    }
  }
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _state );
  }
}

void
Clerk::route_begin ()
{
  if ( _state.is_running () ) {
    return;
  }
  if ( !_actor.go_active () ) {
    return;
  }

  _state.set_is_running ( true );
  _state.set_is_aborting ( false );
  _sequence = Sequence::BEGIN;
  state_changed ();
  request_processing ();
}

void
Clerk::route_break ()
{
  if ( !_state.is_running () || _state.is_aborting () ) {
    return;
  }

  _state.set_is_aborting ( true );
  state_changed ();
}

void
Clerk::route_end ()
{
  if ( !_state.is_running () ) {
    return;
  }

  _state.set_is_running ( false );
  _state.set_is_aborting ( false );
  _sequence = Sequence::NONE;
  _actor.go_idle ();
  state_changed ();
}

void
Clerk::process ()
{
  // -- Update state
  state_update_blocks ();

  // -- Process routing requests
  if ( !_route_requests.is_empty () ) {
    if ( _route_requests.test_any_unset ( Request::BEGIN ) ) {
      route_begin ();
    }
    if ( _route_requests.test_any_unset ( Request::BREAK ) ) {
      route_break ();
    }
  }

  // -- Process sequence
  if ( _state.is_running () ) {
    while ( !_state.is_aborting () && process_sequence () ) {
    }
    process_sequence_abort ();
  }
}

bool
Clerk::process_sequence ()
{
  const std::uint_fast32_t sequence_pre = _sequence;

  switch ( _sequence ) {
  case Sequence::NONE:
    break;

  case Sequence::BEGIN:
    ++_sequence;
    break;

  case Sequence::FEED_LOCK:
    _feed_pilot_control = _feed_pilot.control ();
    if ( _feed_pilot_control ) {
      ++_sequence;
    } else {
      _state.set_is_aborting ( true );
    }
    break;

  case Sequence::MPATH_LOCK:
    _mpath_pilot_control = _mpath_pilot.control ();
    if ( _mpath_pilot_control ) {
      ++_sequence;
    } else {
      _state.set_is_aborting ( true );
    }
    break;

  case Sequence::FEED_PULL:
    if ( _feed_pilot_control.set_mode ( _feed_mode_backward ) ) {
      ++_sequence;
    } else {
      _state.set_is_aborting ( true );
    }
    break;

  case Sequence::FEED_PULL_WAIT:
    // Wait until pilot stopped feeding
    if ( !_feed_pilot.is_potentially_feeding () ) {
      ++_sequence;
    }
    break;

  case Sequence::FEED_FORWARD:
    if ( _feed_pilot_control.set_mode ( _feed_mode_forward ) ) {
      ++_sequence;
    } else {
      _state.set_is_aborting ( true );
    }
    break;

  case Sequence::FEED_FORWARD_WAIT:
    // Wait until pilot stopped feeding
    if ( !_feed_pilot.is_potentially_feeding () ) {
      ++_sequence;
    }
    break;

  case Sequence::ROUTE_BEGIN:
    // Setup MPath
    {
      _mpath_stream_sequence->setup_sequence ( _state.sequence () );
      sev::lag::Vector2d trans;
      trans[ 0 ] = _mpath_pilot.device_state ()
                       .axes ()[ 0 ]
                       .kinematics_latest ()
                       .position ();
      trans[ 1 ] = _mpath_pilot.device_state ()
                       .axes ()[ 1 ]
                       .kinematics_latest ()
                       .position ();
      _mpath_stream_sequence->setup_translation ( trans );
    }
    // Begin MPath routing
    if ( _mpath_pilot_control.route_begin ( _mpath_stream_sequence, "Wind" ) ) {
      ++_sequence;
    } else {
      _state.set_is_aborting ( true );
    }
    break;

  case Sequence::ROUTE_WAIT:
    // Wait until path routing finished
    if ( !_mpath_pilot.state ().is_running () ) {
      ++_sequence;
    }
    break;

  case Sequence::MPATH_UNLOCK:
    _mpath_pilot_control.release ();
    ++_sequence;
    break;

  case Sequence::FEED_UNLOCK:
    _feed_pilot_control.release ();
    ++_sequence;
    break;

  case Sequence::DONE:
    route_end ();
    break;

  default:
    break;
  }

  return ( _sequence != sequence_pre );
}

void
Clerk::process_sequence_abort ()
{
  if ( !_state.is_aborting () ) {
    return;
  }

  // Abort path routing
  if ( _mpath_pilot_control ) {
    _mpath_pilot_control.break_hard ();
    // Wait until path routing stopped
    if ( _mpath_pilot.state ().is_running () ) {
      return;
    }
    // Release remote lock
    _mpath_pilot_control.release ();
  }

  // Abort feeding
  if ( _feed_pilot_control ) {
    _feed_pilot_control.break_hard ();
    // Wait until feeding stopped
    if ( _feed_pilot.is_potentially_feeding () ) {
      return;
    }
    // Release control
    _feed_pilot_control.release ();
  }

  // Abort finished
  route_end ();
}

void
Clerk::state_update_blocks ()
{
  sev::mem::Flags_Fast8 blocks;
  if ( _state.is_running () ) {
    blocks.set ( State::Block::ROUTING_IN_PROGRESS );
  } else if ( !( _feed_pilot_control || _feed_pilot.control_available () ) ||
              !( _mpath_pilot_control || _mpath_pilot.control_available () ) ) {
    blocks.set ( State::Block::CONTROLLERS_UNAVAILABLE );
  }
  if ( _state.sequence ().rounds_total () == 0 ) {
    blocks.set ( State::Block::SEQUENCE_INVALID_ROUNDS );
  }
  // Alignment
  if ( _mpath_pilot.session_is_good () ) {
    if ( _mpath_pilot.device_state ().axes ().size () > 0 ) {
      if ( !_mpath_pilot.device_state ().axes ()[ 0 ].is_aligned () ) {
        blocks.set ( State::Block::WAGON_AXIS_UNALIGNED );
      }
    }
  }

  // Length
  const double length_total ( _state.sequence ().length_total () );
  if ( length_total < 1.0e-6 ) {
    blocks.set ( State::Block::SEQUENCE_INVALID_LENGTH );
  }
  if ( _mpath_pilot.session_is_good () ) {
    if ( _mpath_pilot.device_state ().axes ().size () > 0 ) {
      double pos = _mpath_pilot.device_state ()
                       .axes ()[ 0 ]
                       .kinematics_latest ()
                       .position ();
      if ( length_total > pos ) {
        blocks.set ( State::Block::SEQUENCE_WAGON_UNDER_MIN );
      }
    }
  }

  if ( _state.blocks () != blocks ) {
    _state.blocks () = blocks;
    dash_io ().notification_set ( Dash_Message::STATE );
    request_processing ();
  }
}

void
Clerk::state_changed ()
{
  state_update_blocks ();
  dash_io ().notification_set ( Dash_Message::STATE );
  request_processing ();
}

} // namespace svs::sequence
