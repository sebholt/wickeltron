/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <snc/device/state/state.hpp>
#include <cstdint>

namespace svs::go_to
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Machine-Go_to" )
, _actor ( this )
, _mpath_pilot ( this )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );

  // Setup actor interface
  _actor.set_break_hard_func ( [ this ] () { route_break (); } );

  _mpath_pilot.session ().set_end_callback (
      [ this ] () { _mpath_pilot_control.release (); } );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _actor.connect_required ();
  _mpath_pilot.connect_required ();

  _mpath_stream_goto = std::make_shared< MPath_Stream > ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Type::REQUEST_TARGET:
    dash_event_request_target ( event_n );
    break;
  case dash::event::out::Type::REQUEST_TARGET_CURRENT:
    dash_event_request_target_current ( event_n );
    break;
  case dash::event::out::Type::REQUEST_RUN:
    dash_event_request_run ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  state_changed ();
  request_processing ();
}

void
Clerk::dash_event_request_target ( Dash_Event & event_n )
{
  auto & cevent =
      static_cast< const dash::event::out::Request_Target & > ( event_n );

  log ().cat ( sev::logt::FL_DEBUG_0, "Target request: ", cevent.position () );

  // Setup MPath
  if ( _mpath_pilot.device_state ().axes ()[ 0 ].is_aligned () ) {
    _state.target_pos ()[ 0 ] = cevent.position ();
    state_changed ();
  }
}

void
Clerk::dash_event_request_target_current ( Dash_Event & event_n )
{
  (void)event_n;
  log ().cat ( sev::logt::FL_DEBUG_0, "Target current request." );

  // Setup MPath
  if ( _mpath_pilot.device_state ().axes ()[ 0 ].is_aligned () ) {
    sev::lag::Vector2d vpos;
    vpos[ 0 ] = _mpath_pilot.device_state ()
                    .axes ()[ 0 ]
                    .kinematics_latest ()
                    .position ();
    vpos[ 1 ] = _mpath_pilot.device_state ()
                    .axes ()[ 1 ]
                    .kinematics_latest ()
                    .position ();
    _state.target_pos () = vpos;
    state_changed ();
  }
}

void
Clerk::dash_event_request_run ( Dash_Event & event_n )
{
  using Request = dash::event::out::Request_Run::Request;

  auto & cevent =
      static_cast< const dash::event::out::Request_Run & > ( event_n );
  switch ( cevent.request () ) {
  case Request::START:
    dash_event_request_run_begin ();
    break;
  case Request::ABORT:
    dash_event_request_run_abort ();
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::dash_event_request_run_begin ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Route begin request." );
  if ( !_state.is_running () ) {
    _route_requests.set ( Route_Request::BEGIN );
    request_processing ();
  }
}

void
Clerk::dash_event_request_run_abort ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Route abort request." );
  if ( _route_requests.test_any ( Route_Request::BEGIN ) ) {
    _route_requests.unset ( Route_Request::BEGIN );
  }
  if ( _state.is_running () ) {
    if ( !_route_requests.test_any ( Route_Request::BREAK ) ) {
      _route_requests.set ( Route_Request::BREAK );
      request_processing ();
    }
  }
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _state );
  }
}

void
Clerk::process ()
{
  update_start_blocks ();

  if ( !_route_requests.is_empty () ) {
    // Routing begin
    if ( _route_requests.test_any_unset ( Route_Request::BEGIN ) ) {
      route_begin ();
    }

    // Routing break / abort
    if ( _route_requests.test_any_unset ( Route_Request::BREAK ) ) {
      route_break ();
    }
  }

  // Route finish
  if ( _state.is_running () ) {
    if ( !_mpath_pilot.state ().is_running () ) {
      log ().cat ( sev::logt::FL_DEBUG_0, "Route finished." );
      // Release lock
      _mpath_pilot_control.release ();
      // Update state
      _state.set_is_running ( false );
      _actor.go_idle ();
      state_changed ();
    }
  }
}

void
Clerk::state_changed ()
{
  update_start_blocks ();
  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::update_start_blocks ()
{
  sev::mem::Flags_Fast8 blocks;
  if ( _state.is_running () ) {
    blocks.set ( State::Block::ROUTING_IN_PROGRESS );
  } else if ( !_mpath_pilot_control && !_mpath_pilot.control_available () ) {
    blocks.set ( State::Block::BRIDGE_UNAVAILABLE );
  }
  if ( _mpath_pilot.session_is_good () ) {
    if ( !_mpath_pilot.device_state ().axes ()[ 0 ].is_aligned () ) {
      blocks.set ( State::Block::WAGON_AXIS_UNALIGNED );
    }
  }

  if ( _state.blocks () != blocks ) {
    _state.blocks () = blocks;
    state_changed ();
    request_processing ();
  }
}

void
Clerk::route_begin ()
{
  // When to reject the request
  if ( _state.is_running () ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Route begin denied.  Already running." );
    return;
  }
  if ( !_state.startable () || !_actor.go_active () ) {
    log ().cat ( sev::logt::FL_DEBUG_0, "Route begin denied.  Not startable." );
    return;
  }
  _mpath_pilot_control = _mpath_pilot.control ();
  if ( !_mpath_pilot_control ) {
    log ().cat (
        sev::logt::FL_DEBUG_0,
        "Route begin denied. Could not acquire mpath controller lock." );
    _actor.go_idle ();
    return;
  }

  // Setup MPath
  {
    _mpath_stream_goto->setup_target_pos ( _state.target_pos () );
    sev::lag::Vector2d vpos;
    vpos[ 0 ] = _mpath_pilot.device_state ()
                    .axes ()[ 0 ]
                    .kinematics_latest ()
                    .position ();
    vpos[ 1 ] = _mpath_pilot.device_state ()
                    .axes ()[ 1 ]
                    .kinematics_latest ()
                    .position ();
    _mpath_stream_goto->setup_current_pos ( vpos );
  }

  // Start mpath
  if ( _mpath_pilot_control.route_begin ( _mpath_stream_goto, "Go_to" ) ) {
    log ().cat ( sev::logt::FL_DEBUG_0, "Route begin!" );
    _state.set_is_running ( true );
    state_changed ();
  } else {
    log ().cat ( sev::logt::FL_DEBUG_0, "Route begin failed." );
    _mpath_pilot_control.release ();
    _actor.go_idle ();
  }

  request_processing ();
}

void
Clerk::route_break ()
{
  if ( !_state.is_running () ) {
    return;
  }
  _mpath_pilot_control.break_hard ();
}

} // namespace svs::go_to
