/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/lag/vector2.hpp>
#include <sev/mem/flags.hpp>
#include <cstdint>

namespace svs::go_to
{

class State
{
  public:
  // -- Types

  enum class Stream_State : std::uint8_t
  {
    NONE,
    PREPARE,
    RUNNING,
    FINISHED,
  };

  struct Block
  {
    /// @brief Invalid state (needs update)
    static constexpr std::uint8_t INVALID = ( 1 << 0 );
    static constexpr std::uint8_t ROUTING_IN_PROGRESS = ( 1 << 1 );
    static constexpr std::uint8_t BRIDGE_UNAVAILABLE = ( 1 << 2 );
    static constexpr std::uint8_t WAGON_AXIS_UNALIGNED = ( 1 << 3 );
  };

  // -- Construction

  State ();

  void
  reset ();

  // -- Start blocks

  bool
  startable () const
  {
    return _blocks.is_empty ();
  }

  sev::mem::Flags_Fast8 &
  blocks ()
  {
    return _blocks;
  }

  const sev::mem::Flags_Fast8 &
  blocks () const
  {
    return _blocks;
  }

  // -- Run state

  bool
  is_running () const
  {
    return _is_running;
  }

  void
  set_is_running ( bool flag_n )
  {
    _is_running = flag_n;
  }

  // -- Target position

  sev::lag::Vector2d &
  target_pos ()
  {
    return _target_pos;
  }

  const sev::lag::Vector2d &
  target_pos () const
  {
    return _target_pos;
  }

  public:
  // -- Attributes
  sev::mem::Flags_Fast8 _blocks;
  bool _is_running = false;
  sev::lag::Vector2d _target_pos;
};

} // namespace svs::go_to
