/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/fac/mpath/office/client_n.hpp>
#include <svs/go_to/dash_events.hpp>
#include <svs/go_to/mpath_stream.hpp>
#include <svs/go_to/state.hpp>
#include <memory>

namespace svs::go_to
{

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  struct Dash_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  struct Route_Request
  {
    static constexpr std::uint8_t BEGIN = ( 1 << 0 );
    static constexpr std::uint8_t BREAK = ( 1 << 1 );
  };

  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_request_target ( Dash_Event & event_n );

  void
  dash_event_request_target_current ( Dash_Event & event_n );

  void
  dash_event_request_run ( Dash_Event & event_n );

  void
  dash_event_request_run_begin ();

  void
  dash_event_request_run_abort ();

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  protected:
  // -- Utility

  void
  state_changed ();

  void
  update_start_blocks ();

  void
  route_begin ();

  void
  route_break ();

  private:
  // -- State
  snc::svs::ctl::actor::office::Client_Actor _actor;
  State _state;

  // -- Resources
  sev::mem::Flags_Fast8 _route_requests;
  snc::svs::fac::mpath::office::Client_N< 2 > _mpath_pilot;
  snc::svs::fac::mpath::office::Client_N< 2 >::Control _mpath_pilot_control;
  std::shared_ptr< MPath_Stream > _mpath_stream_goto;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace svs::go_to
