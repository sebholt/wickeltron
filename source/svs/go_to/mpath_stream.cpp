/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "mpath_stream.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>

namespace svs::go_to
{

MPath_Stream::MPath_Stream () {}

void
MPath_Stream::setup_target_pos ( const sev::lag::Vector2d & pos_n )
{
  _pos_target = pos_n;
}

void
MPath_Stream::setup_current_pos ( const sev::lag::Vector2d & pos_n )
{
  _pos_current = pos_n;
}

bool
MPath_Stream::open ()
{
  if ( is_open () ) {
    return true;
  }

  _pos_cleaned_target = _pos_target;
  {
    double angle_current = sev::lag::radians_in_two_pi ( _pos_current[ 1 ] );
    double angle_target = sev::lag::radians_in_two_pi ( _pos_target[ 1 ] );
    double delta = ( angle_target - angle_current );
    if ( delta > sev::lag::pi_d ) {
      delta = sev::lag::two_pi_d - delta;
    } else if ( delta < -sev::lag::pi_d ) {
      delta = sev::lag::two_pi_d + delta;
    }

    _pos_cleaned_target[ 1 ] = ( _pos_current[ 1 ] + delta );
  }
  _pos_cleaned_approach = _pos_cleaned_target;
  _pos_cleaned_approach[ 0 ] += _length_approach;
  sev::math::assign_larger< double > ( _pos_cleaned_approach[ 0 ], 0.0 );

  _next_section = Section::SPEED;
  set_state_good ();

  return is_open ();
}

void
MPath_Stream::close ()
{
  if ( !is_open () ) {
    return;
  }
  set_state_closed ();
}

snc::mpath::Element *
MPath_Stream::read ()
{
  DEBUG_ASSERT ( is_good () );

  switch ( _next_section ) {
  case Section::SPEED:
    _next_section = Section::GOTO_APPROACH;
    {
      auto & elem = _element_variant.emplace< snc::mpath::elem::Speed > ();
      elem.speed ().set_value ( 100000.0 );
      return &elem;
    }

  case Section::GOTO_APPROACH:
    _next_section = Section::GOTO_TARGET;
    return &_element_variant.emplace< snc::mpath::d2::elem::Curve_Linear > (
        _pos_cleaned_approach );

  case Section::GOTO_TARGET:
    _next_section = Section::DONE;
    return &_element_variant.emplace< snc::mpath::d2::elem::Curve_Linear > (
        _pos_cleaned_target );

  case Section::DONE:
    break;
  }

  // Finish stream
  set_state_finished ();
  return &_element_variant.emplace< snc::mpath::elem::Stream_End > ();
}

} // namespace svs::go_to
