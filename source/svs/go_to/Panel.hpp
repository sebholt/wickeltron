/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <svs/go_to/dash_events.hpp>
#include <svs/go_to/state.hpp>

namespace svs::go_to
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties
  Q_PROPERTY ( bool startable READ startable NOTIFY startableChanged )
  Q_PROPERTY ( bool running READ running NOTIFY runningChanged )
  Q_PROPERTY (
      double beginPosition READ beginPosition NOTIFY beginPositionChanged )

  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- State interface

  const svs::go_to::State
  state () const
  {
    return _state;
  }

  // -- State change

  void
  state_changed () override;

  // -- Prop: Startable

  bool
  startable () const
  {
    return _prop.startable;
  }

  Q_SIGNAL
  void
  startableChanged ();

  // -- Prop: running

  bool
  running () const
  {
    return _prop.running;
  }

  Q_SIGNAL
  void
  runningChanged ();

  // -- Prop: beginPosition

  double
  beginPosition () const
  {
    return _prop.begin_position;
  }

  Q_SIGNAL void
  beginPositionChanged ();

  // -- Request interface

  Q_INVOKABLE
  void
  setBeginPosition ( double position_n );

  Q_INVOKABLE
  void
  setBeginToCurrent ();

  Q_INVOKABLE
  void
  goToBegin ();

  Q_INVOKABLE
  void
  routeAbort ();

  private:
  // -- Attributes
  svs::go_to::State _state;
  struct
  {
    bool startable = false;
    bool running = false;
    double begin_position = 0.0;
  } _prop;

  // -- Office event io
  sev::event::Pool< dash::event::out::Request_Target > _event_pool_target;
  sev::event::Pool< dash::event::out::Request_Target_Current >
      _event_pool_target_current;
  sev::event::Pool< dash::event::out::Request_Run > _event_pool_run;
};

} // namespace svs::go_to
