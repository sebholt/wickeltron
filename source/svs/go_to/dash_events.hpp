/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include "state.hpp"
#include <snc/svp/dash/events.hpp>

namespace svs::go_to::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, svs::go_to::State >;

} // namespace svs::go_to::dash::event::in

namespace svs::go_to::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t REQUEST_TARGET = 0;
  static constexpr std::uint_fast32_t REQUEST_TARGET_CURRENT = 1;
  static constexpr std::uint_fast32_t REQUEST_RUN = 2;
};

using Out = snc::svp::dash::event::Out;

class Request_Target : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST_TARGET;

  Request_Target ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _position = 0.0;
  }

  double
  position () const
  {
    return _position;
  }

  void
  set_position ( double position_n )
  {
    _position = position_n;
  }

  private:
  // -- Attributes
  double _position = 0.0;
};

class Request_Target_Current : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST_TARGET_CURRENT;

  Request_Target_Current ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

/// @brief Routing begin / abort request
///
class Request_Run : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST_RUN;

  enum class Request
  {
    NONE,
    START,
    ABORT
  };

  Request_Run ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _request = Request::NONE;
  }

  Request
  request () const
  {
    return _request;
  }

  void
  set_request ( Request request_n )
  {
    _request = request_n;
  }

  private:
  Request _request;
};

} // namespace svs::go_to::dash::event::out
