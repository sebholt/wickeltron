/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <svs/go_to/clerk.hpp>

namespace svs::go_to
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Machine-Go_to" )
, _event_pool_target ( office_io ().epool_tracker () )
, _event_pool_target_current ( office_io ().epool_tracker () )
, _event_pool_run ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< svs::go_to::Clerk > ();
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _prop.startable, state ().startable () ) ) {
    emit startableChanged ();
  }
  if ( sev::change ( _prop.running, state ().is_running () ) ) {
    emit runningChanged ();
  }
  if ( sev::change ( _prop.begin_position, state ().target_pos ()[ 0 ] ) ) {
    emit beginPositionChanged ();
  }
}

void
Panel::setBeginPosition ( double position_n )
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_target );
    event->set_position ( position_n );
    office_io ().submit ( event );
  }
}

void
Panel::setBeginToCurrent ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  office_io ().submit ( office_io ().acquire ( _event_pool_target_current ) );
}

void
Panel::goToBegin ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_run );
    event->set_request ( dash::event::out::Request_Run::Request::START );
    office_io ().submit ( event );
  }
}

void
Panel::routeAbort ()
{
  if ( !office_session_is_good () ) {
    return;
  }
  {
    auto * event = office_io ().acquire ( _event_pool_run );
    event->set_request ( dash::event::out::Request_Run::Request::ABORT );
    office_io ().submit ( event );
  }
}

} // namespace svs::go_to
