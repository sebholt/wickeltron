/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "state.hpp"

namespace svs::go_to
{

State::State ()
{
  reset ();
}

void
State::reset ()
{
  _blocks.assign ( Block::INVALID );
  _is_running = false;
  _target_pos.fill ( 0.0 );
}

} // namespace svs::go_to
