/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/lag/vector2.hpp>
#include <snc/mpath/d2/types.hpp>
#include <snc/mpath_stream/read.hpp>
#include <variant>

namespace svs::go_to
{

class MPath_Stream : public snc::mpath_stream::Read
{
  public:
  // -- Types

  enum Section
  {
    SPEED,
    GOTO_APPROACH,
    GOTO_TARGET,
    DONE
  };

  // -- Construction

  MPath_Stream ();

  // -- Positions

  void
  setup_target_pos ( const sev::lag::Vector2d & pos_n );

  void
  setup_current_pos ( const sev::lag::Vector2d & pos_n );

  // --- Abstract interface

  bool
  open () override;

  void
  close () override;

  snc::mpath::Element *
  read () override;

  private:
  // -- Attributes
  Section _next_section = Section::DONE;

  sev::lag::Vector2d _pos_cleaned_approach;
  sev::lag::Vector2d _pos_cleaned_target;
  double _length_approach = 0.5;
  sev::lag::Vector2d _pos_current;
  sev::lag::Vector2d _pos_target;

  std::variant< snc::mpath::elem::Speed,
                snc::mpath::d2::elem::Curve_Linear,
                snc::mpath::elem::Stream_End >
      _element_variant;
};

} // namespace svs::go_to
