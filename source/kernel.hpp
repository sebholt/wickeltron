/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <sev/logt/server.hpp>
#include <sev/logt/sinks/ostream_stdout.hpp>
#include <sev/thread/tracker.hpp>
#include <sevq/thread/statistics/ListModel.hpp>
#include <Dash.hpp>
#include <QCoreApplication>
#include <QObject>
#include <QQmlApplicationEngine>

class Kernel : public QObject
{
  public:
  // -- Construction

  Kernel ( QCoreApplication & qapp_n );

  // -- Accessors

  sev::logt::Context const &
  log () const
  {
    return _log;
  }

  sev::thread::Tracker &
  thread_tracker ()
  {
    return _thread_tracker;
  }

  // -- Utility
  private:
  void
  parse_args ( QCoreApplication & qapp_n );

  void
  set_log_mask ( sev::logt::Flags flags_n );

  Q_SLOT
  void
  qml_warnings ( const QList< QQmlError > & warnings_n );

  private:
  // -- Attributes
  // - Log server
  sev::logt::sinks::OStream_StdOut _log_sink;
  sev::logt::Server _log_server;
  // - Resources
  sev::logt::Context _log;
  sev::thread::Tracker _thread_tracker;
  sevq::thread::statistics::ListModel _threads_list_model;
  // - Dash
  Dash _dash;
  // - QML
  sev::logt::Context _log_context_qml;
  sev::logt::Context _log_context_qml_error;
  sev::logt::Context _log_context_qml_script;
  QQmlApplicationEngine _qml_engine;
};
