/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#pragma once

#include <snc/device/statics/statics.hpp>
#include <snc/svp/dash/BasicDash.hpp>
#include <snc/svs/ctl/align_axis/dash/Panel.hpp>
#include <snc/svs/ctl/autostart/dash/Panel.hpp>
#include <snc/svs/ctl/axis_feed/dash/Panel.hpp>
#include <snc/svs/ctl/axis_move/dash/Panel.hpp>
#include <snc/svs/ctl/axis_speed/dash/Panel.hpp>
#include <snc/svs/ctl/lookout/dash/Panel.hpp>
#include <snc/svs/ctl/machine_res/dash/Panel.hpp>
#include <snc/svs/fac/axis_align/dash/Panel.hpp>
#include <snc/svs/fac/axis_feed/dash/Panel.hpp>
#include <snc/svs/fac/axis_move/dash/axes_n.hpp>
#include <snc/svs/fac/controls/dash/Panel.hpp>
#include <snc/svs/fac/machine_startup/dash/Panel.hpp>
#include <snc/svs/fac/mpath/dash/Panel_N.hpp>
#include <snc/svs/fac/serial_device/dash/Panel.hpp>
#include <snc/svs/fac/tracker/dash/Panel.hpp>
#include <svs/go_to/Panel.hpp>
#include <svs/sequence/Panel.hpp>

namespace dash
{

class AxisCommon : public QObject
{
  Q_OBJECT

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( snc::device::qt::axes::Axis * info READ info NOTIFY infoChanged )

  public:
  // -- Construction

  AxisCommon ( int index_n, snc::device::qt::Info * info_n );

  Q_SLOT
  void
  updateAxisInfo ();

  // -- Index

  int
  index () const
  {
    return _index;
  }

  // -- State

  snc::device::qt::axes::Axis *
  info ()
  {
    return _info.get ();
  }

  Q_SIGNAL
  void
  infoChanged ();

  private:
  // -- Attributes
  int _index = 0;
  snc::device::qt::Info * _machineInfo = nullptr;
  std::shared_ptr< snc::device::qt::axes::Axis > _info;
  QObject * _statics = nullptr;
};

class AxisWagon : public AxisCommon
{
  Q_OBJECT

  Q_PROPERTY ( QObject * align READ align CONSTANT )
  Q_PROPERTY ( QObject * move READ move CONSTANT )
  Q_PROPERTY ( QObject * speed READ speed CONSTANT )

  public:
  // -- Construction

  AxisWagon ( snc::svp::dash::Dash & dash_n, snc::device::qt::Info * info_n )
  : AxisCommon ( 0, info_n )
  , _align_pilot ( dash_n, index () )
  , _align ( dash_n, index () )
  , _move_pilot ( dash_n, index () )
  , _move ( dash_n, index () )
  , _speed ( dash_n, index () )
  {
  }

  // -- Accessors

  snc::svs::ctl::align_axis::dash::Panel *
  align ()
  {
    return &_align;
  }

  snc::svs::ctl::axis_move::dash::Panel *
  move ()
  {
    return &_move;
  }

  snc::svs::ctl::axis_speed::dash::Panel *
  speed ()
  {
    return &_speed;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_align::dash::Panel _align_pilot;
  snc::svs::ctl::align_axis::dash::Panel _align;

  snc::svs::fac::axis_move::dash::Panel _move_pilot;
  snc::svs::ctl::axis_move::dash::Panel _move;
  snc::svs::ctl::axis_speed::dash::Panel _speed;
};

class AxisSubstrate : public AxisCommon
{
  Q_OBJECT

  Q_PROPERTY ( QObject * move READ move CONSTANT )
  Q_PROPERTY ( QObject * speed READ speed CONSTANT )

  public:
  // -- Construction

  AxisSubstrate ( snc::svp::dash::Dash & dash_n,
                  snc::device::qt::Info * info_n )
  : AxisCommon ( 1, info_n )
  , _move_pilot ( dash_n, index () )
  , _move ( dash_n, index () )
  , _speed ( dash_n, index () )
  {
  }

  // -- Accessors

  snc::svs::ctl::axis_move::dash::Panel *
  move ()
  {
    return &_move;
  }

  snc::svs::ctl::axis_speed::dash::Panel *
  speed ()
  {
    return &_speed;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_move::dash::Panel _move_pilot;
  snc::svs::ctl::axis_move::dash::Panel _move;
  snc::svs::ctl::axis_speed::dash::Panel _speed;
};

class AxisWire : public AxisCommon
{
  Q_OBJECT

  Q_PROPERTY ( QObject * move READ move CONSTANT )
  Q_PROPERTY ( QObject * speed READ speed CONSTANT )
  Q_PROPERTY ( QObject * feed READ feed CONSTANT )
  Q_PROPERTY ( QObject * feedPilot READ feedPilot CONSTANT )

  static constexpr std::uint_fast32_t sensor_index = 1;

  public:
  // -- Construction

  AxisWire ( snc::svp::dash::Dash & dash_n, snc::device::qt::Info * info_n )
  : AxisCommon ( 2, info_n )
  , _move_pilot ( dash_n, index () )
  , _move ( dash_n, index () )
  , _speed ( dash_n, index () )
  , _feed_pilot ( dash_n, index (), sensor_index )
  , _feed ( dash_n, index () )
  {
  }

  // -- Accessors

  snc::svs::ctl::axis_move::dash::Panel *
  move ()
  {
    return &_move;
  }

  snc::svs::ctl::axis_speed::dash::Panel *
  speed ()
  {
    return &_speed;
  }

  snc::svs::fac::axis_feed::dash::Panel *
  feedPilot ()
  {
    return &_feed_pilot;
  }

  snc::svs::ctl::axis_feed::dash::Panel *
  feed ()
  {
    return &_feed;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_move::dash::Panel _move_pilot;
  snc::svs::ctl::axis_move::dash::Panel _move;
  snc::svs::ctl::axis_speed::dash::Panel _speed;

  snc::svs::fac::axis_feed::dash::Panel _feed_pilot;
  snc::svs::ctl::axis_feed::dash::Panel _feed;
};

} // namespace dash

class Dash : public snc::svp::dash::BasicDash
{
  Q_OBJECT

  Q_PROPERTY ( QObject * tracker READ tracker CONSTANT )
  Q_PROPERTY ( QObject * machine_startup READ machine_startup CONSTANT )
  Q_PROPERTY ( QObject * machine_res READ machine_res CONSTANT )
  Q_PROPERTY ( QObject * axisWagon READ axisWagon CONSTANT )
  Q_PROPERTY ( QObject * axisSubstrate READ axisSubstrate CONSTANT )
  Q_PROPERTY ( QObject * axisWire READ axisWire CONSTANT )
  Q_PROPERTY ( QObject * sequence READ sequence CONSTANT )
  Q_PROPERTY ( QObject * go_to READ go_to CONSTANT )

  // -- Types
  private:
  using Super = snc::svp::dash::BasicDash;

  public:
  // -- Construction

  Dash ( const sev::logt::Reference & log_parent_n,
         sev::thread::Tracker * thread_tracker_n,
         QObject * qparent_n = nullptr );

  ~Dash ();

  // -- Accessors

  snc::svs::fac::tracker::dash::Panel *
  tracker ()
  {
    return &_tracker;
  }

  snc::svs::fac::machine_startup::dash::Panel *
  machine_startup ()
  {
    return &_machine_startup;
  }

  snc::svs::ctl::machine_res::dash::Panel *
  machine_res ()
  {
    return &_machine_res;
  }

  // - Axes

  dash::AxisWagon *
  axisWagon ()
  {
    return &_axis_wagon;
  }

  dash::AxisSubstrate *
  axisSubstrate ()
  {
    return &_axis_substrate;
  }

  dash::AxisWire *
  axisWire ()
  {
    return &_axis_wire;
  }

  // - Routing

  svs::sequence::Panel *
  sequence ()
  {
    return &_sequence;
  }

  svs::go_to::Panel *
  go_to ()
  {
    return &_go_to;
  }

  protected:
  // -- Session interface

  void
  session_begin () override;

  private:
  // -- Attributes
  snc::svs::ctl::lookout::dash::Panel _lookout;
  snc::svs::ctl::autostart::dash::Panel _autostart;

  snc::svs::fac::serial_device::dash::Panel _serial_device;
  snc::svs::fac::tracker::dash::Panel _tracker;

  snc::svs::fac::controls::dash::Panel _factor_controls;
  snc::svs::fac::machine_startup::dash::Panel _machine_startup;
  snc::svs::ctl::machine_res::dash::Panel _machine_res;

  dash::AxisWagon _axis_wagon;
  dash::AxisSubstrate _axis_substrate;
  dash::AxisWire _axis_wire;

  snc::svs::fac::mpath::dash::Panel_N< 2 > _factor_mpath;
  svs::sequence::Panel _sequence;
  svs::go_to::Panel _go_to;
};
