/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include <application_config.hpp>
#include <kernel.hpp>
#include <QGuiApplication>
#include <QQuickStyle>
#include <memory>

int
main ( int argc, char * argv[] )
{
  QGuiApplication qapp ( argc, argv );
  qapp.setApplicationName ( QString ( PROGRAM_NAME ).toLower () );
  qapp.setApplicationVersion ( VERSION );

  QQuickStyle::setStyle ( "Fusion" );

  Kernel kernel ( qapp );

  return qapp.exec ();
}
