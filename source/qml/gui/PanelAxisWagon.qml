import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

PanelAxis {
  id: panel
  property var axis: dash.axisWagon

  title: qsTr("Wagon")

  GridLayout {
    columns: 2
    columnSpacing: 36

    Layout.fillWidth: false

    RowLayout {
      Layout.fillWidth: true
      HeadLabelH { text: qsTr("Position") }
      Item { Layout.fillWidth: true }
      HeadLabelH { text: "mm" }
    }
    RowLayout {
      Layout.fillWidth: true
      HeadLabelH { text: "Speed" }
      Item { Layout.fillWidth: true }
      HeadLabelH { text: "mm/s" }
    }

    NumberLabel {
      value: axis.info.state.position
      precision: 4
      Layout.alignment: Qt.AlignRight | Qt.AlignBottom
    }
    NumberLabel {
      value: axis.info.state.speed
      Layout.alignment: Qt.AlignRight | Qt.AlignBottom
    }

    AxisStepButtons {
      axis: panel.axis
      valueTextPrecision: 4
      values: [axis.info.lengthPerStep, 0.01, 0.1, 1.0, 10.0]
    }
    AxisSpeedSliders {
      axis: panel.axis
    }
  }

  Section {
    title: qsTr("Align")
    WagonAlign{}
  }
}
