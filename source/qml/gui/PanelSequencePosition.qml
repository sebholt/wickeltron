import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Panel {
  id: panel

  title: qsTr("Sequence position")

  GridLayout {
    columns: 3
    columnSpacing: 6

    Layout.fillWidth: false;

    Label { text: qsTr("Begin") }
    TextField {
      id: beginField

      property double valueMin: 0.0
      property double valueMax: dash.axisWagon.info.length
      property int precision: 3


      Connections {
        target: dash.go_to
        function onBeginPositionChanged() {
          beginField.text = beginField.valueString(dash.go_to.beginPosition)
        }
      }

      function valueString(value) {
        return Number(value).toLocaleString(panel.locale, "f", precision )
      }

      Layout.fillWidth: true;

      enabled: dash.go_to.startable
      selectByMouse: true
      font.family : "monospace"
      horizontalAlignment: TextInput.AlignRight

      placeholderText: qsTr("?")
      text: valueString(dash.go_to.beginPosition)

      onEditingFinished: {
        try {
          var val = Number.fromLocaleString(panel.locale, text)
        } catch ( err ) {
          val = NaN
        }

        if (isNaN(val)) {
          val = valueMin
        } else if (val < valueMin) {
          val = valueMin
        } else if (val > valueMax) {
          val = valueMax
        }

        dash.go_to.setBeginPosition(val);
        text = valueString(val)
      }
    }
    Label { text: qsTr("mm"); }


    Item { implicitWidth: 1 }
    Button {
      text: qsTr("Current position")
      Layout.fillWidth: true;
      enabled: dash.go_to.startable
      onClicked: dash.go_to.setBeginToCurrent()
    }
    Item { implicitWidth: 1 }
  }
}
