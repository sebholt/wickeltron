import QtQml 2.11
import QtQuick.Extras 1.4

StatusIndicator {
  property int length: 16

  implicitWidth: length
  implicitHeight: length

  color: "darkBlue"
}
