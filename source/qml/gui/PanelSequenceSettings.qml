import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

Panel {
  id: panel

  property var axis: dash.axisSubstrate

  title: qsTr("Sequence settings")

  Component.onCompleted:{
    setupSequence()
  }

  function setupSequence() {
    dash.sequence.setupSequence (
      speedField.value,
      leadField.value / 1000.0,
      roundsField.value )
  }

  GridLayout {
    columns: 3
    columnSpacing: 6

    Label { text: qsTr("Speed") }
    TextField {
      id: speedField

      property double value: 0.0
      property double valueMin: 0.0
      property double valueMax: dash.axisSubstrate.info.speedMaxRoundsPerMinute
      property int precision: 1

      selectByMouse: true
      font.family : "monospace"
      horizontalAlignment: TextInput.AlignRight

      placeholderText: qsTr("?")
      text: Number(valueMax).toLocaleString(panel.locale, "f", precision )

      validator: DoubleValidator {
        bottom: speedField.valueMin
        top: speedField.valueMax
        decimals: speedField.precision
      }

      onTextChanged: {
        var val = Number.fromLocaleString(panel.locale, text)
        if (val > valueMax) {
          value = valueMax
          text = Number(valueMax).toLocaleString(panel.locale, "f", precision)
        } else if (val < valueMin) {
          value = valueMin
          text = Number(valueMin).toLocaleString(panel.locale, "f", precision)
        } else {
          value = val
        }
      }
      onValueChanged: setupSequence()
    }
    Label { text: qsTr("rpm"); }


    Label { text: qsTr("Lead") }
    TextField {
      id: leadField

      property int value: Number.fromLocaleString(panel.locale, text)
      property int valueMin: 0
      property int valueMax: 10*1000

      selectByMouse: true
      font.family : "monospace"
      horizontalAlignment: TextInput.AlignRight

      placeholderText: qsTr("?")
      text: "200"

      validator: IntValidator {
        bottom: leadField.valueMin
        top: leadField.valueMax
      }

      onAcceptableInputChanged: {
        if (value > valueMax) {
          text = valueMax.toString()
        }
        if (value < valueMin) {
          text = valueMin.toString()
        }
      }
      onValueChanged: setupSequence()
    }
    Label { text: qsTr("µm"); }


    Label { text: qsTr("Rounds") }
    TextField {
      id: roundsField

      property int value:  Number.fromLocaleString(panel.locale, text)
      property int valueMin: 0
      property int valueMax: 1000*1000

      selectByMouse: true
      font.family : "monospace"
      horizontalAlignment: TextInput.AlignRight

      placeholderText: qsTr("?")
      text: "10"

      validator: IntValidator {
        bottom: roundsField.valueMin
        top: roundsField.valueMax
      }

      onAcceptableInputChanged: {
        if (value > valueMax) {
          text = valueMax.toString()
        }
        if (value < valueMin) {
          text = valueMin.toString()
        }
      }
      onValueChanged: setupSequence()
    }
    Item { implicitWidth:1 }


    Label { text: qsTr("Length") }
    NumberLabel {
      Layout.alignment: Qt.AlignRight

      precision: 0
      value: leadField.value * roundsField.value
      valueMin: 0.0
      valueMax: leadField.valueMax * roundsField.valueMax
    }
    Label { text: qsTr("µm"); }
  }
}
