import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Extras 1.4

Panel {
  title: qsTr("Sequence control")

  ColumnLayout {
    spacing: 16

    Layout.fillWidth: true

    RowLayout {
      spacing: 16
      Layout.fillWidth: true
      Layout.fillHeight: false

      StackLayout {
        currentIndex: dash.go_to.running? 1 : 0

        Layout.fillWidth: true
        Layout.fillHeight: true

        Button {
          text: qsTr("Go to begin")
          implicitHeight: implicitWidth
          enabled: dash.go_to.startable
          Layout.fillWidth: true
          onClicked: dash.go_to.goToBegin()
        }

        Button {
          text: qsTr("Stop")
          Layout.fillWidth: true
          Layout.fillHeight: true
          enabled: dash.go_to.running
          onPressed: {
            dash.go_to.routeAbort()
          }
        }
      }

      StackLayout {
        currentIndex: dash.sequence.running ? 1 : 0

        Layout.fillWidth: true
        Layout.fillHeight: true

        Button {
          id: startButton
          text: qsTr("Start winding")
          Layout.fillWidth: true
          Layout.fillHeight: true
          enabled: dash.sequence.startable
          onClicked: dash.sequence.routeBegin()
        }

        Button {
          text: qsTr("Stop")
          Layout.fillWidth: true
          Layout.fillHeight: true
          enabled: dash.sequence.running || dash.go_to.running
          onPressed: {
            dash.sequence.routeAbort()
          }
        }
      }
    }

    Button {
      text: qsTr("?")
      Layout.fillWidth: false
      enabled: !dash.sequence.running && !dash.go_to.running && !dash.sequence.startable

      ToolTip.visible: pressed
      ToolTip.text: {
        var res = ""
        var blocks = dash.sequence.blockReasons;
        for (var ii=0; ii != blocks.length; ii++) {
          if (ii != 0) {
            res += "\n"
          }
          res += "- " + blocks[ii]
        }
        return res;
      }
    }

  }
}
