import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Pane {
  id: panel

  default property alias childrenList: layContent.children
  property var title: "Panel title"

  contentWidth: contentRoot.implicitWidth
  contentHeight: contentRoot.implicitHeight
  padding: 0

  ColumnLayout {
    id: contentRoot
    spacing: 0

    anchors.fill: parent

    // -- Title
    Label {
      id: titleLabel
      text: panel.title
      font.bold: true
      font.pointSize: 12
    }
    Item {
      implicitHeight: 4
      implicitWidth: 1
    }
    Rectangle {
      implicitHeight: 1
      implicitWidth: 10
      color: titleLabel.color
      Layout.fillWidth: true
    }
    Item {
      implicitHeight: 10
      implicitWidth: 1
    }

    // -- Content
    ColumnLayout {
      id: layContent
      spacing: 16
      Layout.leftMargin: 6
      Layout.rightMargin: 24
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    }
  }
}
