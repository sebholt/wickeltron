import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

GridLayout {
  property var axisFeed: dash.axisWire.feed
  property var axisFeedPilot: dash.axisWire.feedPilot

  columns: 2
  columnSpacing: 16

  Button {
    id: button
    text: qsTr("None")
    enabled: axisFeed.available

    onClicked: axisFeed.feedNone()
  }
  WireFeedState{
    length: button.height
    active: axisFeedPilot.modeNone
  }

  Button {
    text: qsTr("Feed")
    enabled: axisFeed.available

    onClicked: axisFeed.feedForward()
  }
  WireFeedState{
    length: button.height
    active: axisFeedPilot.modeForward
  }

  Button {
    text: qsTr("Tighten")
    enabled: axisFeed.available

    onClicked: axisFeed.feedBackward()
  }
  WireFeedState{
    length: button.height
    active: axisFeedPilot.modeBackward
  }
}
