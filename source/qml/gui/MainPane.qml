import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Pane {
  id: root

  property int panelHSpace: 32
  property int panelVSpace: 32

  contentWidth: layRoot.implicitWidth
  contentHeight: layRoot.implicitHeight
  padding: 12

  //background: Rectangle {
  //  color: root.palette.window
  //}

  RowLayout {
    id: layRoot
    spacing: panelHSpace
    anchors.fill: parent

    ColumnLayout {
      spacing: panelVSpace
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillHeight: true

      RowLayout {
        spacing: panelHSpace

        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: true

        PanelSequenceControl {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: true
          Layout.minimumWidth: implicitWidth
        }
        PanelSequencePosition {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: true
          Layout.minimumWidth: implicitWidth
        }
        PanelSequenceSettings {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: true
          Layout.minimumWidth: implicitWidth
        }
      }

      RowLayout {
        spacing: panelHSpace

        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: true

        PanelAxisWagon {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: true
          Layout.minimumWidth: implicitWidth
        }
        PanelAxisSubstrate {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: true
          Layout.minimumWidth: implicitWidth
        }
        PanelAxisWire {
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: true
          Layout.minimumWidth: implicitWidth
        }
      }
    }
    
    ColumnLayout {
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      PanelDevice {
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }
  }
}