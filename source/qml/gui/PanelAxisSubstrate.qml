import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import SncQ 1.0

PanelAxis {
  id: panel
  property var axis: dash.axisSubstrate

  title: qsTr("Substrate")

  GridLayout {
    columns: 2
    columnSpacing: 36

    Layout.fillWidth: false

    RowLayout {
      Layout.fillWidth: true
      HeadLabelH { text: qsTr("Angle") }
      Item { Layout.fillWidth: true }
      HeadLabelH { text: "°" }
    }
    RowLayout {
      Layout.fillWidth: true
      HeadLabelH { text: "Speed" }
      Item { Layout.fillWidth: true }
      HeadLabelH { text: "rpm" }
    }

    NumberLabel {
      value: axis.info.state.degrees
      Layout.alignment: Qt.AlignRight | Qt.AlignBottom
    }
    NumberLabel {
      value: axis.info.state.speedRoundsPerMinute
      Layout.alignment: Qt.AlignRight | Qt.AlignBottom
    }

    AxisStepButtons {
      axis: panel.axis
      values: [1.0, 45.0, 90.0, 135.0, 180.0]
    }
    AxisSpeedSliders {
      axis: panel.axis
    }
  }
}
