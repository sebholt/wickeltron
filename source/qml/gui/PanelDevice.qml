import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Extras 1.4
import SncQ 1.0

Panel {
  title: qsTr("Device state")

  property var leftAlign: Qt.AlignLeft | Qt.AlignBottom
  property var rightAlign: Qt.AlignRight | Qt.AlignBottom

  RowLayout {
    spacing: 16
    Layout.alignment: leftAlign
    Label {
      id: connectionLabel
      text: qsTr("Connected"); font.bold: true;
    }
    StatusIndicator {
      implicitWidth: connectionLabel.height
      implicitHeight: connectionLabel.height
      color: "green"
      active: dash.machine_startup.connected
    }
  }

  Section {
    title: qsTr("USB bitrates")

    ColumnLayout {
      spacing: 2
      Layout.alignment: leftAlign
      RowLayout {
        Label { text: qsTr("In"); }
        Item{Layout.fillWidth: true;}
        Label { text: qsTr("bits/s"); }
      }
      NumberLabel {
        value: dash.tracker.info.state.serialBitrateIn;
        valueMax: 1000 * 1000 * 1000;
        Layout.alignment: rightAlign
      }
    }

    ColumnLayout {
      spacing: 2
      Layout.alignment: leftAlign
      RowLayout {
        Label { text: qsTr("Out"); }
        Item{Layout.fillWidth: true;}
        Label { text: qsTr("bits/s"); }
      }
      NumberLabel {
        value: dash.tracker.info.state.serialBitrateOut;
        valueMax: 1000 * 1000 * 1000;
        Layout.alignment: rightAlign
      }
    }

    ColumnLayout {
      spacing: 2
      Layout.alignment: leftAlign
      RowLayout {
        Label { text: qsTr("Total"); }
        Item{Layout.fillWidth: true;}
        Label { text: qsTr("bits/s"); }
      }
      NumberLabel {
        value: dash.tracker.info.state.serialBitrateTotal;
        valueMax: 1000 * 1000 * 1000;
        Layout.alignment: rightAlign
      }
    }
  }

  Section {
    title: qsTr("Latency in ms")

    NumberLabel {
      value: dash.tracker.info.state.latency;
      valueMax: 1000 * 10;
      Layout.alignment: rightAlign
    }
  }

  Section {
    title: qsTr("Sensors")

    PanelStateSensorsI1 {
      model: dash.tracker.info.sensors.i1
    }
  }

  Section {
    title: qsTr("Threads")

    Layout.fillWidth: true
    Layout.fillHeight: true

    ThreadStats {
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }
}
