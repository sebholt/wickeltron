import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtQuick.Extras 1.4

RowLayout {
  property var axisAlign: dash.axisWagon.align

  spacing: 16

  Button {
    id: button
    text: axisAlign.aligning ? qsTr( "Stop" ) : qsTr( "Align" )
    enabled: axisAlign.available

    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

    onClicked: {
      if (axisAlign.aligning) {
        axisAlign.alignAbort();
      } else {
        axisAlign.alignBegin();
      }
    }
  }
  StatusIndicator {
    implicitWidth: button.height
    implicitHeight: button.height
    color: "darkGreen"
    active: dash.axisWagon.info.state.aligned
  }
}
