import QtQuick.Layouts 1.11
import SncQ 1.0

ColumnLayout {
  id: section
  property var title: qsTr("Title")

  spacing: 5

  HeadLabelV {
    text: section.title
  }
}
