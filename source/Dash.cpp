/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "Dash.hpp"
#include <sev/utility.hpp>
#include <snc/emb/config/config.hpp>
#include <snc/math/stepping.hpp>
#include <snc/usb/device_query.hpp>
#include <QFile>

namespace dash
{

AxisCommon::AxisCommon ( int index_n, snc::device::qt::Info * info_n )
: _index ( index_n )
, _machineInfo ( info_n )
{
  updateAxisInfo ();
}

void
AxisCommon::updateAxisInfo ()
{
  if ( sev::change ( _info,
                     _machineInfo->axes ()->getOrCreateShared ( _index ) ) ) {
    emit infoChanged ();
  }
}

} // namespace dash

Dash::Dash ( const sev::logt::Reference & log_parent_n,
             sev::thread::Tracker * thread_tracker_n,
             QObject * qparent_n )
: Super ( { log_parent_n, "Dash" }, thread_tracker_n, qparent_n )
, _lookout ( *this )
, _autostart ( *this )
, _serial_device ( *this )
, _tracker ( *this )
, _factor_controls ( *this )
, _machine_startup ( *this )
, _machine_res ( *this )
, _axis_wagon ( *this, _tracker.info () )
, _axis_substrate ( *this, _tracker.info () )
, _axis_wire ( *this, _tracker.info () )
, _factor_mpath ( *this )
, _sequence ( *this )
, _go_to ( *this )
{
}

Dash::~Dash () = default;

void
Dash::session_begin ()
{
  Super::session_begin ();

  // Setup device configuration
  {
    QFile jsonFile ( ":/devices/Wickeltron.json" );
    DEBUG_ASSERT ( jsonFile.exists () );
    if ( jsonFile.open ( QIODevice::ReadOnly ) ) {
      qint64 maxSize = 1024 * 1024 * 10;
      QByteArray data = jsonFile.read ( maxSize );
      DEBUG_ASSERT ( !data.isEmpty () );
      _autostart.set_default_device_statics_json ( data.toStdString () );
    } else {
      DEBUG_ASSERT ( false );
    }
  }
}
