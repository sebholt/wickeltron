/// wickeltron: Numeric coiling machine controller.
///
/// \copyright See LICENSE-wickeltron.txt file

#include "kernel.hpp"
#include <snc/qt/qml.hpp>
#include <application_config.hpp>
#include <QCommandLineParser>
#include <QQmlContext>
#include <QQmlEngine>

Kernel::Kernel ( QCoreApplication & qapp_n )
: QObject ( &qapp_n )
, _log_sink ( LOG_MASK_DEFAULT )
, _log_server ( LOG_MASK_DEFAULT )
, _log ( _log_server, "Kernel" )
, _threads_list_model ( this, &_thread_tracker )
, _dash ( _log, &_thread_tracker, this )
, _log_context_qml ( _log, "QML" )
, _log_context_qml_error ( _log_context_qml, "Error" )
, _log_context_qml_script ( _log_context_qml, "Script" )
, _qml_engine ( this )
{
  _log_sink.set_colors_enabled ( true );
  _log_server.sink_register ( &_log_sink );

  // -- Parse command line arguments
  parse_args ( qapp_n );

  // -- Setup qml engine
  {
    // Connect error logging
    _qml_engine.setOutputWarningsToStandardError ( false );
    connect ( &_qml_engine,
              &QQmlApplicationEngine::warnings,
              this,
              &Kernel::qml_warnings );

    // Add import path
    for ( const auto & path : snc::qt::qml_import_paths () ) {
      _qml_engine.addImportPath ( path );
    }
    if ( auto olog = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      olog << "QML import paths:\n";
      for ( const QString & item : _qml_engine.importPathList () ) {
        olog << "  " << item.toStdString () << "\n";
      }
    }

    snc::qt::qml_register_types ();

    // Register C++ objects in QML context
    {
      QQmlContext * qml_context = _qml_engine.rootContext ();
      qml_context->setContextProperty ( "dash", &_dash );
      qml_context->setContextProperty ( "threadsListModel",
                                        &_threads_list_model );
    }
  }

  // -- End the application whe the Dash ended
  QObject::connect ( &_dash,
                     &Dash::stopped,
                     &qapp_n,
                     &QCoreApplication::quit,
                     Qt::QueuedConnection );

  // -- Start dash
  _dash.start ();

  // -- Load root script
  _qml_engine.load ( QUrl ( "qrc:///qml/gui/Window.qml" ) );
}

void
Kernel::qml_warnings ( const QList< QQmlError > & warnings_n )
{
  if ( auto olog = _log_context_qml_error.ostr ( sev::logt::FL_WARNING ) ) {
    olog << "-- QML engine warnings --";
    for ( const QQmlError & qerr : warnings_n ) {
      olog << "\nURL: ";
      olog << qerr.url ().toString ().toStdString ();
      olog << "\nLine,Column: ";
      olog << qerr.line ();
      olog << ",";
      olog << qerr.column ();
      olog << "\n";
      olog << qerr.description ().toStdString ();
      olog << "\n";
    }
  }
}

void
Kernel::set_log_mask ( sev::logt::Flags flags_n )
{
  _log_sink.set_mask ( flags_n );
  _log_server.set_mask ( flags_n );
}

void
Kernel::parse_args ( QCoreApplication & qapp_n )
{
  QCommandLineParser parser;
  parser.setApplicationDescription ( "Coil winding machine control software." );
  parser.addHelpOption ();
  parser.addVersionOption ();

  // A boolean option with multiple names (-f, --force)
  QCommandLineOption debugOption (
      QStringList () << "d"
                     << "debug",
      QCoreApplication::translate ( "main", "Enable debug mode." ) );
  parser.addOption ( debugOption );

  // Process the actual command line arguments given by the user
  parser.process ( qapp_n );

  if ( parser.isSet ( debugOption ) ) {
    set_log_mask ( sev::logt::FLG_EWIP | sev::logt::FL_DEBUG );
  } else {
    set_log_mask ( sev::logt::FLG_EWIP );
  }
}
