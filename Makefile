
all: debug release

debug:
	cd ~/build/cbench2-control/debug && make -j8

release:
	cd ~/build/cbench2-control/release && make -j8

